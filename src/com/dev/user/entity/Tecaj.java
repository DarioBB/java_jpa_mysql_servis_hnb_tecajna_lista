package com.dev.user.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "tecaj")
public class Tecaj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@Column(name="id")
	private int id;
	
	@Column(name="sifra")
	private String sifra;
	
	@Column(name="valuta")
	private String valuta;
	
	@Column(name="jedinica")
	private String jedinica;
	
	@Column(name="kupovni")
	private String kupovni;
	
	@Column(name="srednji")
	private String srednji;
	
	@Column(name="prodajni")
	private String prodajni;
	
	@Column(name="on_date")
	private java.sql.Date on_date;
	
	@Column(name="created")
	private Timestamp created;
	

	public Tecaj(String sifra, String valuta, String jedinica, String kupovni, String srednji, String prodajni, java.sql.Date on_date, Timestamp created)
	{
		super();
		this.sifra = sifra;
		this.valuta = valuta;
		this.jedinica = jedinica;
		this.kupovni = kupovni;
		this.srednji = srednji;
		this.prodajni = prodajni;
		this.on_date = on_date;
		this.created = created;
	}
	
	public Tecaj()
	{
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public String getValuta() {
		return valuta;
	}

	public void setValuta(String valuta) {
		this.valuta = valuta;
	}

	public String getJedinica() {
		return jedinica;
	}

	public void setJedinica(String jedinica) {
		this.jedinica = jedinica;
	}

	public String getKupovni() {
		return kupovni;
	}

	public void setKupovni(String kupovni) {
		this.kupovni = kupovni;
	}

	public String getSrednji() {
		return srednji;
	}

	public void setSrednji(String srednji) {
		this.srednji = srednji;
	}

	public String getProdajni() {
		return prodajni;
	}

	public void setProdajni(String prodajni) {
		this.prodajni = prodajni;
	}

	public java.sql.Date getOn_date() {
		return on_date;
	}

	public void setOn_date(java.sql.Date on_date) {
		this.on_date = on_date;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tecaj other = (Tecaj) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Tecaj [id=" + id + ", sifra=" + sifra + ", valuta=" + valuta
				+ ", jedinica=" + jedinica + ", kupovni=" + kupovni
				+ ", srednji=" + srednji + ", prodajni=" + prodajni
				+ ", on_date=" + on_date + ", created=" + created + "]";
	}
	
}
