package com.dev.user.app;
import java.net.*;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.io.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.dev.user.entity.Tecaj;
import com.dev.user.controller.Conversion;
import com.dev.user.controller.Helper;

public class App implements Download, Save {
	
	@Override
	public void save_data(String[][] data) {
		// TODO Auto-generated method stub
		
		if(data.length > 0)
		{
			EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("hnb");
			EntityManager entitymanager = emfactory.createEntityManager( );
			entitymanager.getTransaction( ).begin( );
			
			for(int i = 0; i < data.length; i++){
				try{
			        Date on_date = new SimpleDateFormat("ddMMyy").parse(data[i][4]);
			        
			        Helper h = new Helper();
			        on_date = h.convertJavaDateToSqlDate(on_date);
			        
			        if(data[i][0].substring(0, 3) != "")
			        {
					  Query query = entitymanager.createQuery("SELECT t.id FROM Tecaj t WHERE t.sifra = :sifra1 and t.on_date = :on_date");
					  query.setParameter("sifra1", data[i][0].substring(0, 3));
					  query.setParameter("on_date", h.convertJavaDateToSqlDate(on_date));
					  System.out.println(query);
					  //query.getSingleResult();
					  
			            if(query.getResultList().size() == 0)
			            {
				        	Tecaj tj = new Tecaj();
							tj.setSifra(data[i][0].substring(0, 3));
							tj.setValuta(data[i][0].substring(3, 6));
							tj.setJedinica(data[i][0].substring(6, 9));
							tj.setKupovni(data[i][1]);
							tj.setSrednji(data[i][2]);
							tj.setProdajni(data[i][3]);
							tj.setOn_date(h.convertJavaDateToSqlDate(on_date));
							tj.setCreated(new Timestamp(System.currentTimeMillis()));
							entitymanager.persist(tj);
							
							System.out.println("\nPodaci su uspješno spremljeni za valutu "+data[i][0].substring(3, 6)+", datum: "+h.convertJavaDateToSqlDate(on_date)+"");
			            }
			            else
			            {
			            	System.out.println("\nPodaci su već ranije spremljeni za datum "+h.convertJavaDateToSqlDate(on_date)+"");
			            }
			        }
			        //new TecajnaLista(data[i][0].substring(0, 3), data[i][0].substring(3, 6), data[i][0].substring(6, 9), data[i][1], data[i][2], data[i][3], on_date);
		        }
	        	catch (NullPointerException | ParseException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				} /*catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
	        }
			entitymanager.getTransaction().commit();
			
			entitymanager.close();
			emfactory.close();
		}
	}
	
	@Override
	public String[][] download_data(String url) {
		// TODO Auto-generated method stub
		URL oracle = null;
		try {
			oracle = new URL(url);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        URLConnection yc = null;
		try {
			yc = oracle.openConnection();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
        BufferedReader in = null;
        String[] parts = null;
        String[][] data = new String[56][56];
		try {
			in = new BufferedReader(new InputStreamReader(
			                        yc.getInputStream()));
			String inputLine;
	        int j = 0;
	        int k = 0;
	        try {
				while ((inputLine = in.readLine()) != null) 
				{
					//String[] parts = null;
					if(j > 0)
					{
				        //System.out.println(inputLine+"\n");
				    	try{
				    		parts = inputLine.split("      ");
				    		
				    		int m = 0;
				    		for(int i = 0; i < parts.length; i++){
				            	//System.out.println(""+i+":"+parts[i].trim()+"\n");
				            	//System.out.println("k: "+k+", i: "+i);
				            	data[k][i] = parts[i].trim();
				            	m++;
				            }
				    		//data[k][m] = url.substring(25, 36);
				    		//data[k][m+1] = url.substring(26, 32);
				    		data[k][m] = url.substring(26, 32);
				    	} catch (java.lang.NullPointerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				    	k++;
					}
					j++;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			System.out.println("\nDatoteka sa podacima o tečaju nije pronađena\n");
		}
        
		return data;
	}
	
	@Override
	public void download_all_data(List<Date> dates) {
		
		DateFormat formatter;
		formatter = new SimpleDateFormat("ddMMyy");
		
		String[][] data = null;
		//Object[][] data_for_save = new Object[][];
		for(int z=0;z<dates.size();z++){
		    Date lDate =(Date)dates.get(z);
		    String ds = formatter.format(lDate);    
		    //System.out.println("Datum: " + ds);
		    
		    String url = "http://www.hnb.hr/tecajn/f"+ds+".dat";

		    data = this.download_data(url);
		    //data_for_save = data;
			this.save_data(data);
		}
	}
	
	public App()
	{
		//
	}
	
	public App(String url)
	{
		String[][] data = this.download_data(url);
		this.save_data(data);
	}
	
	public App(String date_from, String date_until)
	{

		Helper h = new Helper();
		
		////String until_date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
		List<Date> dates = h.getDaysBetweenDates(date_from, date_until);
		//List<Date> dates = h.getDaysBetweenDates("22/10/2010", until_date);
		this.download_all_data(dates);
	}
	
	public App(String in_value, String out_value, String parse_date, Integer amount) throws ParseException
	{
		Helper h = new Helper();
		
		/* POCETAK KODA - POZIV KONVERZIJE VALUTA*/
		Conversion c = new Conversion();
		Date conversion_date = new SimpleDateFormat("dd/MM/yyyy").parse(parse_date);
		conversion_date = h.convertJavaDateToSqlDate(conversion_date);
		
		String conversion_value = c.conversion(in_value, out_value, conversion_date, amount); // eur u usd
		
		System.out.println("\nVrijednost konverzije na dan "+ conversion_date+": "+conversion_value);
		/* KORAJ KODA - POZIV KONVERZIJE VALUTA*/
	}
	
	public static void main(String[] args) throws IOException, ParseException{
		//System.out.println("test");
		
		App app = new App("http://www.hnb.hr/tecajn/f150915.dat"); //poziv konstruktora sa jednim parametrom - download i spremanje jedne definirane tecajne liste sa hnb-a
		//App app2 = new App("01/01/1997", "22/09/2015"); // OPREZ (izvrsava se do 10-tak min) - poziv konstruktora sa dva parametra - download i spremanje cijele arhive tecajnih listi sa hnb-a
		App app3 = new App("978", "840", "16/09/2015", 10); //poziv konstruktora sa 4 parametra - izracunavanje konverzije
	}

}
