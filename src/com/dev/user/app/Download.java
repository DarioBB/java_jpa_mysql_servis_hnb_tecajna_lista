package com.dev.user.app;

import java.util.Date;
import java.util.List;

public interface Download {
	
	public String[][] download_data(String url);
	
	public void download_all_data(List<Date> dates);
	
}
