package com.dev.user.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Helper {

	public java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
	    return new java.sql.Date(date.getTime());
	}
	
	public List<Date> getDaysBetweenDates(String str_date, String end_date)
	{
		List<Date> dates = new ArrayList<Date>();

		DateFormat formatter; 

		formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = (Date)formatter.parse(str_date);
			endDate = (Date)formatter.parse(end_date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		long interval = 24* 1000 * 60 * 60; // 1 hour in millis
		long endTime = endDate.getTime() ; // create your endtime here, possibly using Calendar or Date
		long curTime = startDate.getTime() + TimeUnit.HOURS.toMillis(12);
		while (curTime <= endTime) {
		    dates.add(new Date(curTime));
		    curTime += interval;
		}
		
		return dates;
	}
}
