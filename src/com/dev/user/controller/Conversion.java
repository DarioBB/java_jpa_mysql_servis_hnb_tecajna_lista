package com.dev.user.controller;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class Conversion {

	public String srednji_tecaj_ulazna_valuta;
	public String srednji_tecaj_izlazna_valuta;
	public Double calculation;
	public String result;
	
	public String conversion(String ulazna_valuta, String izlazna_valuta, Date datum, Integer kolicina){
		
		LinkedHashMap<String,Object> map = this.getConversion(ulazna_valuta, izlazna_valuta, datum);
		
		srednji_tecaj_ulazna_valuta = null;
		srednji_tecaj_izlazna_valuta = null;
		
		for (Map.Entry entry : map.entrySet()) {
		    //System.out.println("valuta: "+entry.getKey() + ", srednji tecaj: " + entry.getValue());
    		if(entry.getKey().toString().equals(ulazna_valuta)){
    			srednji_tecaj_ulazna_valuta = entry.getValue().toString();
    		}
    		else if(entry.getKey().toString().equals(izlazna_valuta)){
    			srednji_tecaj_izlazna_valuta = entry.getValue().toString();
    		}
		}
		//System.out.println("Srednji tečaj ulazna valuta: "+srednji_tecaj_ulazna_valuta);
		//System.out.println("Srednji tečaj izlazna valuta: "+srednji_tecaj_izlazna_valuta);
		
		if(srednji_tecaj_ulazna_valuta != null && srednji_tecaj_izlazna_valuta != null)
		{
			srednji_tecaj_ulazna_valuta = srednji_tecaj_ulazna_valuta.replace(",", ".");
			srednji_tecaj_izlazna_valuta = srednji_tecaj_izlazna_valuta.replace(",", ".");
			
			calculation = (Double.parseDouble(srednji_tecaj_ulazna_valuta) / Double.parseDouble(srednji_tecaj_izlazna_valuta)) * kolicina;
			result = calculation.toString().replace(".", ",");
		}
		else 
		{
			result = "Ulazni podaci su neodgovarajući. \nProvjerite šifru ulazne i izlazne valute te unešeni datum za koji ste pokušali dobiti konverziju.";
		}
		//System.out.println(result);
		return result;
		
	}
	
	public LinkedHashMap<String,Object> getConversion(String ulazna_valuta, String izlazna_valuta, Date datum)
	{
        LinkedHashMap<String,Object> map = new LinkedHashMap();

			
		  EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("hnb");
		  EntityManager entitymanager = emfactory.createEntityManager();
		  entitymanager.getTransaction().begin();

		  Query query = entitymanager.createQuery("SELECT t.id, t.sifra, t.srednji FROM Tecaj t WHERE t.sifra IN(:sifra1, :sifra2) and t.on_date = :on_date");
		  query.setParameter("sifra1", ulazna_valuta);
		  query.setParameter("sifra2", izlazna_valuta);
		  query.setParameter("on_date", datum);
		  System.out.println(query);
		  //entitymanager.getTransaction().commit();
		  List results = query.getResultList();
		  Iterator it = results.iterator( );  
		  
		  int i = 0;
		  while (it.hasNext( )) {  
		  
		     Object[] result = (Object[])it.next();
		     //System.out.println("\n\n test: "+result[1].toString());
		     map.put(result[1].toString(), result[2].toString());
		     i++;
		  }
		
		  entitymanager.close();
		  emfactory.close();
		
		return map;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((srednji_tecaj_ulazna_valuta == null) ? 0
						: srednji_tecaj_ulazna_valuta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conversion other = (Conversion) obj;
		if (srednji_tecaj_ulazna_valuta == null) {
			if (other.srednji_tecaj_ulazna_valuta != null)
				return false;
		} else if (!srednji_tecaj_ulazna_valuta
				.equals(other.srednji_tecaj_ulazna_valuta))
			return false;
		return true;
	}

}
